package soap

import (
	"encoding/xml"

	"time"
)

// against "unused imports"
var _ time.Time
var _ xml.Name

type AccessKeyLogon struct {
	XMLName xml.Name `xml:"they:Authenticate"`

	AccessKey string `xml:"they:accessKey,omitempty"`
}

type AccessKeyLogonResponse struct {
	SessionID string `xml:"Body>AuthenticateResponse>AuthenticateResult"`
}

type SessionSoap struct {
	client *SOAPClient

	SessionID string
}

func NewSessionSoap(url string, tls bool) *SessionSoap {
	if url == "" {
		url = "https://api.yukiworks.nl/ws/Accounting.asmx"
	}
	client := NewSOAPClient(url, tls)

	return &SessionSoap{
		client: client,
	}
}

func (service *SessionSoap) SetHeader(header interface{}) {
	service.client.SetHeader(header)
}

/* Creates session based on passed access key and returns its ID and external cluster URL */
func (service *SessionSoap) AccessKeyLogon(request *AccessKeyLogon) (*AccessKeyLogonResponse, error) {
	response := new(AccessKeyLogonResponse)
	err := service.client.Call("http://www.theyukicompany.com/Authenticate", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
