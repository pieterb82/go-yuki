package soap

import (
	"encoding/xml"
	"time"
)

var _ time.Time

type ProcessJournal struct {
	XMLName xml.Name `xml:"they:ProcessJournal"`

	SessionID        string `xml:"they:sessionID"`
	AdministrationID string `xml:"they:administrationID"`

	XmlDoc XmlDoc `xml:"urn:xmlns:http://www.theyukicompany.com:journal they:xmlDoc>Journal"`
}

type XmlDoc struct {
	AdministrationID        string         `xml:"AdministrationID,omitempty"`
	AdministrationCoCNumber string         `xml:"AdministrationCoCNumber,omitempty"`
	DocumentSubject         string         `xml:"DocumentSubject,omitempty"`
	JournalType             JournalType    `xml:"JournalType,omitempty"`
	ProjectID               string         `xml:"ProjectID,omitempty"`
	ProjectCode             string         `xml:"ProjectCode,omitempty"`
	JournalEntries          []JournalEntry `xml:"JournalEntry"`
}

type JournalType string

const (
	GeneralJournal      JournalType = "GeneralJournal"
	EndOfYearCorrection JournalType = "EndOfYearCorrection"
	FicalCorrection     JournalType = "FiscalCorrection"
)

type JournalEntry struct {
	ContactName string  `xml:"ContactName,omitempty"`
	ContactCode string  `xml:"ContactCode,omitempty"`
	DossierName string  `xml:"DossierName,omitempty"`
	DossierCode string  `xml:"DossierCode,omitempty"`
	EntryDate   string  `xml:"EntryDate,omitempty"`
	GLAccount   string  `xml:"GLAccount,omitempty"`
	Amount      float64 `xml:"Amount,omitempty"`
	Description string  `xml:"Description,omitempty"`
}

type ProcessJournalResponse struct {
	Result string `xml:"Body>Result"`
}

type ProcessJournalSoap struct {
	client *SOAPClient

	SessionID string
}

func NewProcessJournalSoap(url string, tls bool) *ProcessJournalSoap {
	if url == "" {
		url = "https://api.yukiworks.nl/ws/Accounting.asmx"
	}
	client := NewSOAPClient(url, tls)

	return &ProcessJournalSoap{
		client: client,
	}
}

/* Creates session based on passed access key and returns its ID and external cluster URL */
func (service *ProcessJournalSoap) ProcessJournalRequest(request *ProcessJournal) (*ProcessJournalResponse, error) {
	response := new(ProcessJournalResponse)
	err := service.client.Call("http://www.theyukicompany.com/ProcessJournal", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
