package yuki

func NewAccountingService(api *API) *AccountingService {
	return &AccountingService{api: api}
}

type AccountingService struct {
	api *API
}
