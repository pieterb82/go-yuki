package main

import (
	"bitbucket.org/pieterb82/go-yuki/soap"
	"fmt"
	"os"
)

func main() {
	session := soap.NewSessionSoap("", true)
	logon := &soap.AccessKeyLogon{
		AccessKey: "d667401b-6f37-4d66-9008-986fee39b559",
	}

	logonResp, err := session.AccessKeyLogon(logon)
	if err != nil {
		panic(err)
	}

	session.SessionID = logonResp.SessionID

	journal := soap.NewProcessJournalSoap("", true)

	processJounal := &soap.ProcessJournal{
		SessionID:        session.SessionID,
		AdministrationID: "667210c9-c073-4883-8c11-dcf72608013d",
		XmlDoc: soap.XmlDoc{
			AdministrationID: "667210c9-c073-4883-8c11-dcf72608013d",
			JournalType:      soap.GeneralJournal,
			DocumentSubject:  "Golang Test",
			JournalEntries: []soap.JournalEntry{
				soap.JournalEntry{
					ContactName: "Apple Sales International",
					EntryDate:   "2018-01-09",
					GLAccount:   "45310",
					Amount:      22.22,
					Description: "Inkopen hardware",
				},
				soap.JournalEntry{
					ContactName: "Apple Sales International",
					EntryDate:   "2018-01-09",
					GLAccount:   "45310",
					Amount:      -22.22,
					Description: "Retour hardware",
				},
			},
		},
	}

	processJounalResp, err := journal.ProcessJournalRequest(processJounal)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v", processJounalResp)
	os.Exit(34)
}
