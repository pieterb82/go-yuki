package yuki

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"bitbucket.org/pieterb82/go-yuki/soap"
)

const (
	libraryVersion = "0.0.1"
	userAgent      = "go-yuki/" + libraryVersion
	mediaType      = "text/xml"
	userAgent      = "utf-8"
	sessionTimeout = time.Minute * 30
)

var (
	sessionID    string
	sessionStart time.Time
	accessToken  string
)

func Login() error {
	session := soap.NewSessionSoap("", true)
	logon := &soap.AccessKeyLogon{
		AccessKey: accessToken,
	}

	logonResp, err := session.AccessKeyLogon(logon)
	if err != nil {
		return err
	}

	SessionID = logonResp.SessionID
	return nil
}
