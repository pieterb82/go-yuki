package yuki

import (
	"net/http"
	"net/url"
)

func (s *AccountingService) Authenticate() AuthenticateRequest {
	return AuthenticateRequest{
		api:    s.api,
		method: http.MethodPost,
	}
}

type AuthenticateRequest struct {
	api    *API
	method string
}

func (r *AuthenticateRequest) Method() string {
	return r.method
}

func (r *AuthenticateRequest) SetMethod(method string) {
	r.method = method
}

func (r *AuthenticateRequest) Do() (AuthenticateResponse, error) {

}

type AuthenticateResponse struct {
}
